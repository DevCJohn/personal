import './App.css';
import { FC } from 'react';

import serene from './assets/serene.webp';

const App: FC = () => (
  <div className="App">
    <header className="App-header">
      <img src={serene} className="App-bigpic" alt="A serene view of Grant's Trail" />
    </header>
  </div>
);

export default App;
